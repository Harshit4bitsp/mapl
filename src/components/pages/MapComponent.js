import React, { useEffect } from 'react';

function MapComponent() {
  useEffect(() => {
    const script = document.createElement('script');
    script.src = 'https://apis.mappls.com/advancedmaps/api/3c50dd0dc7669b11f0cbadb8c34a2585/map_sdk?layer=vector&v=3.0&callback=initMap1';
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);

    window.initMap1 = function() {
      const map = new window.mappls.Map('map', {center: [28.3591, 75.5882]});
      map.setZoom(15);
    }   

    return () => {
      document.body.removeChild(script);
      delete window.initMap1;
    };
  }, []);

  return (
    <div id="map" style={{ width: '100%', height: '100vh', margin: 0, padding: 0 }}></div>
  );
}

export default MapComponent;
