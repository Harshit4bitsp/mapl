import React from 'react';
import { Link } from 'react-router-dom';
import MapComponent from './MapComponent'; // Import MapComponent

const containerStyle = {
  display: 'flex',
  height: '100vh',
};

const mapStyle = {
  flex: 2, // Adjust flex value for desired width
  height: '100%',
};

const navStyle = {
  flex: 1, // Adjust flex value for desired width
  height: '100%',
  backgroundColor: '#f0f0f0', // Background color of the navigation bar
  display: 'flex',
  flexDirection: 'column', // Arrange content vertically
  alignItems: 'center', // Center content horizontally
  justifyContent: 'center', // Center content vertically
};


export default function HomePage() {
  return (
    <div style={containerStyle}>
      <div style={navStyle}>
        <div className="text-center">
          <h1 className="main-title home-page-title">Explore as you Travel</h1>
          <Link to="/map">
            <button className="primary-button">View My Map</button>
          </Link>
          <Link to="/">
            <button className="primary-button">Log out</button>
          </Link>
        </div>
      </div>
      <div style={mapStyle}>
        <MapComponent />  {/* Use MapComponent here */}
      </div>
    </div>
  );
}
