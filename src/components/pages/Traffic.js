import React, { useState } from 'react';
import axios from 'axios';
const http = require('http');

const Traffic = () => {
  const [response, setResponse] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();

    const apiKey = '3c50dd0dc7669b11f0cbadb8c34a2585';
    const startCoordinates = '28.6387,77.2197'; // Set start coordinates here
    const destinationCoordinates = '28.7041,77.1025'; // Set destination coordinates here
    const profile = 'driving'; // Set profile here

    // const baseUrl = 'https://cors-anywhere.herokuapp.com/https://apis.mapmyindia.com/advancedmaps/v1/';
    const baseUrl = 'https://apis.mapmyindia.com/advancedmaps/v1/';
    const requestUrl = `${baseUrl}${apiKey}/route_adv/${profile}/${startCoordinates};${destinationCoordinates}`;

    http.get(requestUrl, (res) => {
        let data = '';
      
        res.on('data', (chunk) => {
          data += chunk;
        });
      
        res.on('end', () => {
          setResponse(JSON.stringify(JSON.parse(data), null, 2));
        });
      }).on('error', (err) => {
        setResponse(`Error: ${err.message}`);
      });
  };

  return (
    <div>
      <h1>MapmyIndia API Request</h1>
      <button onClick={handleSubmit}>Get Route</button>
      <div id="response">{response}</div>
    </div>
  );
};

export default Traffic;
