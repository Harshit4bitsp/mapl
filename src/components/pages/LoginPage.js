import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import '../../App.css';

export default function SignInPage() {
    const [accessToken, setAccessToken] = useState('');

    const getAccessToken = async () => {
        const baseUrl = 'https://outpost.mappls.com/api';
        const endpoint = '/security/oauth/token';
        const clientId = '96dHZVzsAuscVqRhYxVyyW5S-Xd9I9gLYOMPbuQuscxKhZUpe0q1scsOc5R2BgrlXIku5dmiIe9lVDCCUoHYRWeG-TlPQbJo';
        const clientSecret = 'lrFxI-iSEg_R1w3MdBP9ATgooX32nRkzaVrNlimU0YZ52RYNO-WZOtxcMtWE5AlIQ0lXnUn9TSWbVJnsK1MGwozqnfoqoR8aRsocidk9lCM=';

        const requestBody = {
            grant_type: 'client_credentials',
            client_id: clientId,
            client_secret: clientSecret
        };

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + btoa(`${clientId}:${clientSecret}`)
            },
            body: JSON.stringify(requestBody)
        };

        // try {
        //     const response = await fetch(baseUrl + endpoint, requestOptions);
        //     const data = await response.json();
        //     setAccessToken(data.access_token);
        //     // Handle success, maybe redirect or store token in local storage
        // } catch (error) {
        //     console.error('Error fetching access token:', error);
        //     // Handle error, maybe display an error message to the user
        // }
    };

    return (
        <div className="text-center m-5-auto">
            <h2>Sign in to the App</h2>
            <form onSubmit={getAccessToken}>
                <p>
                    <label>Username or email address</label><br/>
                    <input type="text" name="first_name" required />
                </p>
                <p>
                    <label>Password</label>
                    <Link to="/forget-password"><label className="right-label">Forget password?</label></Link>
                    <br/>
                    <input type="password" name="password" required />
                </p>

                <p>
                <button id="sub_btn" type="submit" style={{ color: 'white', textDecoration: 'none', backgroundColor: 'black', border: 'none' }}>
  <Link to="/home" style={{ color: 'white', textDecoration: 'none' }}>Begin Your Journey</Link>
</button>

                </p>
            </form>
            <footer>
                <p>First time? <Link to="/register">Create an account</Link>.</p>
                <p><Link to="/">Back to Homepage</Link>.</p>
            </footer>
        </div>
    );
}