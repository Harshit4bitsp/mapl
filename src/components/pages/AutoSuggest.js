// AutoSuggest.js
import React, { useState } from 'react';
import axios from 'axios';

const AutoSuggest = ({ onTokenGenerate }) => {
    const [query, setQuery] = useState('');
    const [suggestions, setSuggestions] = useState([]);

    const handleChange = async (event) => {
        const value = event.target.value;
        setQuery(value);

        try {
            // Make request to MapMyIndia Geocoding API for suggestions
            const response = await axios.get(`https://atlas.mapmyindia.com/api/places/geocode?address=${value}&region=IND&key=3c50dd0dc7669b11f0cbadb8c34a2585`);
            
            // Update suggestions with response data
            if (response.data && response.data.results) {
                setSuggestions(response.data.results);
            }
        } catch (error) {
            console.error('Error fetching suggestions:', error);
        }
    };

    const handleSelectAddress = (address) => {
        // Generate token from the selected address
        const token = `Token for ${address.formatted_address}`;
        // Pass the token back to the parent component
        onTokenGenerate(token);
    };

    return (
        <div>
            <input
                type="text"
                value={query}
                onChange={handleChange}
                placeholder="Enter address"
            />
            <ul>
                {suggestions.map((suggestion, index) => (
                    <li key={index} onClick={() => handleSelectAddress(suggestion)}>
                        {suggestion.formatted_address}
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default AutoSuggest;
